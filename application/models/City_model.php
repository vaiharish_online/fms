<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class City_model extends CI_Model
{

    function addNewCity($cityInfo)
    {
        $this->db->trans_start();
        $this->db->insert('adm_city', $cityInfo);
        
        $insert_id = $this->db->insert_id();
        
        $this->db->trans_complete();
        
        return $insert_id;
    }

    function getStatelist()
    {
        $this->db->from('adm_state');
        $query = $this->db->get();
        
        return $query->result();
    }

    function cityListingCount($searchText = '')
    {
        $this->db->select('BaseTbl.id, BaseTbl.city_name, s.state_name, BaseTbl.pincode, BaseTbl.city_code, BaseTbl.state_id, BaseTbl.district_name');
        $this->db->from('adm_city as BaseTbl');
        $this->db->join('adm_state as s', 's.state_id = BaseTbl.state_id','left');
        if(!empty($searchText)) {
            $likeCriteria = "(BaseTbl.city_name  LIKE '%".$searchText."%'
                            OR  BaseTbl.district_name  LIKE '%".$searchText."%'
                            OR  BaseTbl.pincode  LIKE '%".$searchText."%'
                            OR  s.state_name  LIKE '%".$searchText."%')";
            $this->db->where($likeCriteria);
        }
        $this->db->where('BaseTbl.status', 1);
        // $this->db->where('BaseTbl.roleId !=', 1);
        $query = $this->db->get();
        
        return count($query->result());
    }

     /**
     * This function is used to get the user listing count
     * @param string $searchText : This is optional search text
     * @param number $page : This is pagination offset
     * @param number $segment : This is pagination limit
     * @return array $result : This is result
     */

    // function cityListing($searchText = '', $page, $segment)
    function cityListing()
    {
        // echo 'here'; die;
        $this->db->select('BaseTbl.id, BaseTbl.city_name, s.state_name, BaseTbl.city_code, BaseTbl.district_name, BaseTbl.state_id, BaseTbl.pincode');
        $this->db->from('adm_city as BaseTbl');
        $this->db->join('adm_state as s', 's.state_id = BaseTbl.state_id','left');
        // if(!empty($searchText)) {
        //     $likeCriteria = "(BaseTbl.city_name  LIKE '%".$searchText."%'
        //                     OR  BaseTbl.district_name  LIKE '%".$searchText."%'
        //                     OR  BaseTbl.pincode  LIKE '%".$searchText."%'
        //                     OR  s.state_name  LIKE '%".$searchText."%')";
        //     $this->db->where($likeCriteria);
        // }
        $this->db->where('BaseTbl.status', 1);
        // $this->db->where('BaseTbl.roleId !=', 1);
        // $this->db->limit($page, $segment);
        $query = $this->db->get();
        
        $result = $query->result();        
        return $result;
    }

     /**
     * This function used to get user information by id
     * @param number $userId : This is user id
     * @return array $result : This is user information
     */
    function getCityInfo($cityId)
    {
        $this->db->select('id, city_id, city_name, pincode, city_code, state_id, district_name');
        $this->db->from('adm_city');
        $this->db->where('id', $cityId);
        $query = $this->db->get();
        
        return $query->result();
    }

     /**
     * This function is used to update the user information
     * @param array $userInfo : This is users updated information
     * @param number $userId : This is user id
     */
    function editCity($cityInfo, $cityId)
    {
        $this->db->where('id', $cityId);
        $this->db->update('adm_city', $cityInfo);
        
        return TRUE;
    }

    /**
    * This function is used to delete the user information
    * @param number $userId : This is user id
    * @return boolean $result : TRUE / FALSE
    */
    function deleteCity($cityId, $cityInfo)
    {
        $this->db->where('id', $cityId);
        $this->db->update('adm_city', $cityInfo);
        
        return $this->db->affected_rows();
    }

}

