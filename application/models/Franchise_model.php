<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Franchise_model extends CI_Model
{

    function franchiseListingCount($searchText = '')
    {
        $this->db->select('BaseTbl.*');
        $this->db->from('adm_franchise_master as BaseTbl');
        // $this->db->join('adm_state as s', 's.state_id = BaseTbl.state_id','left');
        if(!empty($searchText)) {
            $likeCriteria = "()";
            $this->db->where($likeCriteria);
        }
        // $this->db->where('BaseTbl.status', 1);
        // $this->db->where('BaseTbl.roleId !=', 1);
        $query = $this->db->get();
        
        return count($query->result());
    }

    // function franchiseListing($searchText = '', $page, $segment)
    function franchiseListing()
    {
        // echo 'here'; die;
        $this->db->select('BaseTbl.*');
        $this->db->from('adm_franchise_master as BaseTbl');
        // $this->db->join('adm_state as s', 's.state_id = BaseTbl.state_id','left');
        // if(!empty($searchText)) {
        //     $likeCriteria = "(BaseTbl.city_name  LIKE '%".$searchText."%'
        //                     OR  BaseTbl.district_name  LIKE '%".$searchText."%'
        //                     OR  BaseTbl.pincode  LIKE '%".$searchText."%'
        //                     OR  s.state_name  LIKE '%".$searchText."%')";
        //     $this->db->where($likeCriteria);
        // }
        // $this->db->where('BaseTbl.status', 1);
        // $this->db->where('BaseTbl.roleId !=', 1);
        // $this->db->limit($page, $segment);
        $query = $this->db->get();
        
        $result = $query->result();        
        return $result;
    }

    function addNewFranchise($franchiseInfo)
    {
        $this->db->trans_start();
        $this->db->insert('adm_franchise_master', $franchiseInfo);
        
        $insert_id = $this->db->insert_id();
        
        $this->db->trans_complete();
        
        return $insert_id;
    }

    function getCitylist()
    {
        $this->db->from('adm_city');
        $query = $this->db->get();
        
        return $query->result();
    }

    function getLastUserId(){
        $last_row = $this->db->select('userId')->order_by('userId',"desc")->limit(1)->get('tbl_users')->row();
        return $last_row;
    }

    function getFranchiseInfo($franchiseId)
    {
        $this->db->select('*');
        $this->db->from('adm_franchise_master');
        $this->db->where('id', $franchiseId);
        $query = $this->db->get();
        
        return $query->result();
    }

    function editFranchise($franchiseInfo, $franchiseId, $user_franchiseid)
    {
        // echo $franchiseInfo['photo']; die;
        $imgset = array_key_exists('photo', $franchiseInfo);

        $this->db->where('id', $franchiseId);
        $this->db->update('adm_franchise_master', $franchiseInfo);

        //updating image in users table for franchise image change
        if($imgset == 1){
            $photo = array('photo'=>$franchiseInfo['photo']);
            $this->db->where('email', $user_franchiseid);
            $this->db->update('tbl_users',$photo);
        }
        
        return TRUE;
    }
}

