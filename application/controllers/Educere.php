<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

/**
 * Class : Educere (EducereController)
 * User Class to control all Euducere  related operations.
 * @author : Kumar G
 * @version : 1.1
 * @since : 01 April 2020
 */
class Educere extends BaseController
{
    /**
     * This is default constructor of the class
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Educere_model');
        $this->isLoggedIn();   
    }
    
    /**
     * This function used to load the first screen of the user
     */
    public function index()
    {
        $this->global['pageTitle'] = 'Intellikidz : Educere';        
        $this->loadViews("educere", $this->global, NULL , NULL);
    }

    function educereListing()
    {
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {
            // echo 'test'; die;
            $this->load->model('Educere_model');
        
            // $searchText = $this->input->post('searchText');
            // $data['searchText'] = $searchText;
            
            // $this->load->library('pagination');
            
            // $count = $this->Educere_model->franchiseListingCount($searchText);

            // $returns = $this->paginationCompress ( "franchiseListing/", $count, 5);
            
            // print_r($count);  die;
            
            // $data['franchiseRecords'] = $this->Educere_model->franchiseListing($searchText, $returns["page"], $returns["segment"]);
            $data['educereRecords'] = $this->Educere_model->franchiseListing();
            
            // print_r($data); die;
            $this->global['pageTitle'] = 'Intellikidz : Educere Listing';
            
            $this->loadViews("educere", $this->global, $data, NULL);
        }
    }


    function addNewEducere()
    {
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {
            $this->load->model('Educere_model');
            $data['states'] = $this->Educere_model->getCitylist();
            $data['franchises'] = $this->Educere_model->getFranchiselist();
            
            $this->global['pageTitle'] = 'Intellikidz : Add New Educere';

            $this->loadViews("addNewEducere", $this->global, $data, NULL);
        }
    }

    function addNewEducere1()
    {
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {
            $this->load->library('form_validation');
            
            $this->form_validation->set_rules('fname','Full Name','trim|required|max_length[128]|xss_clean');
            $this->form_validation->set_rules('franchisename','franchisename','trim|required|xss_clean');
            $this->form_validation->set_rules('address','address','trim|required|max_length[250]|xss_clean');
            $this->form_validation->set_rules('dob','dob','trim|required|max_length[30]|xss_clean');
            $this->form_validation->set_rules('cno','cno','trim|required|max_length[40]|xss_clean');
            $this->form_validation->set_rules('email','email','required|max_length[40]|xss_clean');
            $this->form_validation->set_rules('pincode','pincode','trim|required|xss_clean');
            $this->form_validation->set_rules('bgroup','bgroup','trim|required|xss_clean|max_length[128]');
            $this->form_validation->set_rules('gender','gender','trim|required|xss_clean');
            $this->form_validation->set_rules('qualification','qualification','trim|required|xss_clean');
            $this->form_validation->set_rules('year','year','trim|required|xss_clean');
            $this->form_validation->set_rules('ldate','ldate','trim|required|xss_clean');            
            $this->form_validation->set_rules('alevel','alevel','trim|required|xss_clean');
            // $this->form_validation->set_rules('btype','sadress','trim|required|max_length[30]|xss_clean');
            
            if($this->form_validation->run() == FALSE)
            {
                // echo 'faild'; die;
                $this->addNewEducere();
            }
            else
            {
                // print_r($_POST);
                // // print_r($_FILES);
                // die;
    
                if(isset($_FILES['image'])){
                   // print_r($_FILES);
                //    print_r($_POST); die;
                    $errors= array();
                    $file_name = $_FILES['image']['name'];
                    $file_size = $_FILES['image']['size'];
                    $file_tmp = $_FILES['image']['tmp_name'];
                    $file_type = $_FILES['image']['type'];
                    $file_ext=explode('.',$file_name);
                    $file_ext1=strtolower(end($file_ext));

                    $expensions= array("jpeg","jpg","png","PNG");

                    if(in_array($file_ext1,$expensions)=== false){
                        $errors[]="extension not allowed, please choose a JPEG or PNG file.";
                    }

                    if($file_size > 2097152) {
                        $errors[]='File size must be excately 2 MB';
                    }

                    if(empty($errors)==true) {
                        move_uploaded_file($file_tmp,"assets/images/franchise/".$file_name);
                        // echo "Success";
                    }else{
                        print_r($errors);
                    }
                }else{
                    $file_name = 'no-img.png';
                }

                // die;
                $fullname = $this->input->post('fname');
                $qualification = $this->input->post('qualification');
                $email = $this->input->post('email');
                $dob = $this->input->post('dob');
                $contactno = $this->input->post('cno');
                $address = $this->input->post('address');
                $franchise_id = $this->input->post('franchisename');
                $ldate =  $this->input->post('ldate');
                $year = $this->input->post('year');
                $gender = $this->input->post('gender');
                $pincode = $this->input->post('pincode');
                $bgroup = $this->input->post('bgroup');
                $addlevel = $this->input->post('alevel');
                $profilePic = $file_name;
                // $state = 'KA01';

                $dob = new DateTime($dob);
                $levelDate = new DateTime($ldate);
                
                $educereInfo = array(
                    'educere_name'=>$fullname,
                    'franchise_id'=>$franchise_id,
                    'educere_id' =>2,
                    'gender' => $gender,
                    'pincode' => $pincode,
                    'blood_group' =>$bgroup,
                    'dob'=>$dob->format('Y-m-d'),
                    'address' =>$address,
                    'year'=>$year,
                    'photo'=>$profilePic,
                    'contact_number'=>$contactno,
                    'qualification' => $qualification,
                    'email' =>$email
                );
                
                $this->load->model('Educere_model');
                $result = $this->Educere_model->addNewEducere($educereInfo);
                
                if($result > 0)
                {
                    $this->session->set_flashdata('success', 'New Educere created successfully');
                }
                else
                {
                    $this->session->set_flashdata('error', 'Educere creation failed');
                }
                
                redirect('educereListing');
            }
        }
    }

      function editOldEducere($educereId = NULL)
    {
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {
            if($educereId == null)
            {
                redirect('educereListing');
            }
            
            $data['franchises'] = $this->Educere_model->getFranchiselist();
            $data['educereInfo'] = $this->Educere_model->getEducereInfo($educereId);
            
            // print_r($data); die;
            $this->global['pageTitle'] = 'Intellikidz : Edit Educere';
            
            $this->loadViews("editOldEducere", $this->global, $data, NULL);
        }
    }

    function editEducere()
    {
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {
            $this->load->library('form_validation');
            
            $this->form_validation->set_rules('fname','Full Name','trim|required|max_length[128]|xss_clean');
            $this->form_validation->set_rules('franchisename','franchisename','trim|required|xss_clean');
            $this->form_validation->set_rules('address','address','trim|required|max_length[250]|xss_clean');
            $this->form_validation->set_rules('dob','dob','trim|required|max_length[30]|xss_clean');
            $this->form_validation->set_rules('cno','cno','trim|required|max_length[40]|xss_clean');
            $this->form_validation->set_rules('email','email','required|max_length[40]|xss_clean');
            $this->form_validation->set_rules('pincode','pincode','trim|required|xss_clean');
            $this->form_validation->set_rules('bgroup','bgroup','trim|required|xss_clean|max_length[128]');
            $this->form_validation->set_rules('gender','gender','trim|required|xss_clean');
            $this->form_validation->set_rules('qualification','qualification','trim|required|xss_clean');
            $this->form_validation->set_rules('year','year','trim|required|xss_clean');
            $this->form_validation->set_rules('ldate','ldate','trim|required|xss_clean');            
            $this->form_validation->set_rules('alevel','alevel','trim|required|xss_clean');
            // $this->form_validation->set_rules('btype','sadress','trim|required|max_length[30]|xss_clean');
            
            if($this->form_validation->run() == FALSE)
            {
                // echo 'faild'; die;
                $this->editOldEducere();
            }
            else
            {
                // print_r($_POST);    
                // var_dump($_FILES);
                // die;
                $educereId = $this->input->post('id');
                // $franchise_id = $this->input->post('franchise_id');
                $file_name = '';
                if(isset($_FILES['image']) && $_FILES['image']['size']>0){
                    // echo 'ii fdf';
                   // print_r($_FILES);
                //    print_r($_POST); die;
                    $errors= array();
                    $file_name = $_FILES['image']['name'];
                    $file_size = $_FILES['image']['size'];
                    $file_tmp = $_FILES['image']['tmp_name'];
                    $file_type = $_FILES['image']['type'];
                    $file_ext=explode('.',$file_name);
                    $file_ext1=strtolower(end($file_ext));

                    $expensions= array("jpeg","jpg","png","PNG");

                    if(in_array($file_ext1,$expensions)=== false){
                        $errors[]="extension not allowed, please choose a JPEG or PNG file.";
                    }

                    if($file_size > 2097152) {
                        $errors[]='File size must be excately 2 MB';
                    }

                    if(empty($errors)==true) {
                        move_uploaded_file($file_tmp,"assets/images/franchise/".$file_name);
                        // echo "Success";
                    }else{
                        print_r($errors);
                    }
                }

                $fullname = $this->input->post('fname');
                $qualification = $this->input->post('qualification');
                $email = $this->input->post('email');
                $dob = $this->input->post('dob');
                $contactno = $this->input->post('cno');
                $address = $this->input->post('address');
                $franchise_id = $this->input->post('franchisename');
                $ldate =  $this->input->post('ldate');
                $year = $this->input->post('year');
                $gender = $this->input->post('gender');
                $pincode = $this->input->post('pincode');
                $bgroup = $this->input->post('bgroup');
                $addlevel = $this->input->post('alevel');
                // $profilePic = $file_name;


                $dob = new DateTime($dob);
                $levelDate = new DateTime($ldate);
                
                $educereInfo = array(
                    'educere_name'=>$fullname,
                    'franchise_id'=>$franchise_id,
                    'educere_id' =>2,
                    'gender' => $gender,
                    'pincode' => $pincode,
                    'blood_group' =>$bgroup,
                    'dob'=>$dob->format('Y-m-d'),
                    'address' =>$address,
                    'year'=>$year,
                    'contact_number'=>$contactno,
                    'qualification' => $qualification,
                    'email' =>$email
                );

                if(!empty($file_name)){
                     $educereInfo['photo'] = $file_name;
                }

                // print_r($franchiseInfo); die;

                
                $this->load->model('Educere_model');
                $result = $this->Educere_model->editEducere($educereInfo,$educereId);

                
                if($result > 0)
                {
                    $this->session->set_flashdata('success', 'Educere details updated successfully');
                }
                else
                {
                    $this->session->set_flashdata('error', 'Educere details upation failed');
                }
                
                redirect('educereListing');
            }
        }
    }
    
    function pageNotFound()
    {
        $this->global['pageTitle'] = 'Intellikidz : 404 - Page Not Found';
        
        $this->loadViews("404", $this->global, NULL, NULL);
    }
}

?>