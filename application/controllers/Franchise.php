<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

/**
 * Class : User (UserController)
 * User Class to control all user related operations.
 * @author : Kishor Mali
 * @version : 1.1
 * @since : 15 November 2016
 */
class Franchise extends BaseController
{
    /**
     * This is default constructor of the class
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Franchise_model');
        $this->isLoggedIn();   
    }
    
    /**
     * This function used to load the first screen of the user
     */
    public function index()
    {
        $this->global['pageTitle'] = 'Intellikidz : Franchise';        
        $this->loadViews("franchise", $this->global, NULL , NULL);
    }

    function franchiseListing()
    {
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {
            // echo 'test'; die;
            $this->load->model('Franchise_model');
        
            // $searchText = $this->input->post('searchText');
            // $data['searchText'] = $searchText;
            
            // $this->load->library('pagination');
            
            // $count = $this->Franchise_model->franchiseListingCount($searchText);

            // $returns = $this->paginationCompress ( "franchiseListing/", $count, 5);
            
            // print_r($count);  die;
            
            // $data['franchiseRecords'] = $this->Franchise_model->franchiseListing($searchText, $returns["page"], $returns["segment"]);
            $data['franchiseRecords'] = $this->Franchise_model->franchiseListing();
            
            // print_r($data); die;
            $this->global['pageTitle'] = 'Intellikidz : Franchise Listing';
            
            $this->loadViews("franchise", $this->global, $data, NULL);
        }
    }


    function addNewFranchise()
    {
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {
            $this->load->model('Franchise_model');
            $data['states'] = $this->Franchise_model->getCitylist();
            
            $this->global['pageTitle'] = 'Intellikidz : Add New Frachise';

            $this->loadViews("addNewFranchise", $this->global, $data, NULL);
        }
    }

    function addNewFranchise1()
    {
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {
            $this->load->library('form_validation');
            
            $this->form_validation->set_rules('cname','Centre Name','trim|required|max_length[128]|xss_clean');
            $this->form_validation->set_rules('coordinator','coordinator','trim|required|xss_clean|max_length[128]');
            $this->form_validation->set_rules('email','email','required|max_length[40]|xss_clean');
            $this->form_validation->set_rules('dob','dob','trim|required|max_length[30]|xss_clean');
            $this->form_validation->set_rules('mno','mno','trim|required|max_length[40]|xss_clean');
            $this->form_validation->set_rules('saddress','saddress','trim|required|max_length[250]|xss_clean');
            $this->form_validation->set_rules('state','state','trim|required|xss_clean');
            $this->form_validation->set_rules('cdate','cdate','trim|required|xss_clean');
            $this->form_validation->set_rules('cyear','cyear','trim|required|xss_clean');
            $this->form_validation->set_rules('rdate','rdate','trim|required|xss_clean');
            $this->form_validation->set_rules('btype','btype','trim|required|xss_clean');
            // $this->form_validation->set_rules('btype','sadress','trim|required|max_length[30]|xss_clean');
            
            if($this->form_validation->run() == FALSE)
            {
                // echo 'faild'; die;
                $this->addNewFranchise();
            }
            else
            {
                // print_r($_POST);
                // print_r($_FILES);
                // die;
                $cid = $this->input->post('state');
                $this->load->model('City_model');

                //fetching state id from adm_city table
                $stateid = $this->City_model->getCityInfo($cid);

                $stateName = strtoupper(substr($stateid[0]->state_id,0,2));
                $cityName = strtoupper(substr($stateid[0]->city_name,0,3));

                //fetching last inserted userid from tbl_users table
                $lastUserid = $this->Franchise_model->getLastUserId();
                $lastUserid = $lastUserid->userId+1;

                //creating franchise login id 
                $franchiseId = 'IKLPL-'.$stateName.'-'.$cityName.'-'.date("y").'-'.$lastUserid;

                if(isset($_FILES['image'])){
                   // print_r($_FILES);
                //    print_r($_POST); die;
                    $errors= array();
                    $file_name = $_FILES['image']['name'];
                    $file_size = $_FILES['image']['size'];
                    $file_tmp = $_FILES['image']['tmp_name'];
                    $file_type = $_FILES['image']['type'];
                    $file_ext=explode('.',$file_name);
                    $file_ext1=strtolower(end($file_ext));

                    $expensions= array("jpeg","jpg","png","PNG");

                    if(in_array($file_ext1,$expensions)=== false){
                        $errors[]="extension not allowed, please choose a JPEG or PNG file.";
                    }

                    if($file_size > 2097152) {
                        $errors[]='File size must be excately 2 MB';
                    }

                    if(empty($errors)==true) {
                        move_uploaded_file($file_tmp,"assets/images/franchise/".$file_name);
                        // echo "Success";
                    }else{
                        print_r($errors);
                    }
                }else{
                    $file_name = 'no-img.png';
                }

                // die;
                $cname = $this->input->post('cname');
                $coordinator = $this->input->post('coordinator');
                $email = $this->input->post('email');
                $dob = $this->input->post('dob');
                $mobileno = $this->input->post('mno');
                $streetAddress = $this->input->post('saddress');
                $city = $this->input->post('state');
                $commemceDate =  $this->input->post('cdate');
                $commenceYear = $this->input->post('cyear');
                $renewalDate = $this->input->post('rdate');
                $branchType = $this->input->post('btype');
                $profilePic = $file_name;
                // $state = 'KA01';
                // echo $dob;
                // $myDateTime = DateTime::createFromFormat('Y-m-d', $dob);
                // $val = $myDateTime->format('Y-m-d');
                // echo $val;

                $dob = new DateTime($dob);
                $commemceDate = new DateTime($commemceDate);
                $renewalDate = new DateTime($renewalDate);

                // echo $date->format('d.m.Y'); // 31.07.2012
                // echo $date->format('Y-m-d');
                
                
                $franchiseInfo = array('franchise_name'=>$cname,
                'city_id'=> $city, 
                'contact_person_name' =>$coordinator,
                'contact_person_dob' => $dob->format('Y-m-d'),
                'contact_person_email' => $email, 
                'franchise_address' => $streetAddress, 
                'contact_person_mobile' => $mobileno,
                'franchise_commence_date' => $commemceDate->format('Y-m-d'),
                // 'contact_commenct' => $commenceYear,
                'franchise_renewal_date' => $renewalDate->format('Y-m-d'),
                'photo' => $profilePic,
                'status' => 1,
                'franchise_id'=>$franchiseId);

                // contact_person_dob
                // franchise_info
                // franchise_code
                // franchise_commence_date
                // franchise_renewal_date
                // franchise_type_id
                // photo
                // status
                
                $this->load->model('Franchise_model');
                $result = $this->Franchise_model->addNewFranchise($franchiseInfo);

                $name = ucwords($cname);
                // $email = $email;
                $password = 'admin123';
                $roleId = 4;
                $mobile =$mobileno;
                
                $userInfo = array('email'=>$franchiseId, 'password'=>getHashedPassword($password), 'roleId'=>$roleId, 'name'=> $name,
                                    'mobile'=>$mobile, 'createdBy'=>$this->vendorId, 'createdDtm'=>date('Y-m-d H:i:s'), 'photo'=>$file_name);
                
                $this->load->model('user_model');
                $result1 = $this->user_model->addNewUser($userInfo);
                
                if($result1 > 0)
                {
                    $this->session->set_flashdata('success', 'New Franchise created successfully');
                }
                else
                {
                    $this->session->set_flashdata('error', 'Franchise creation failed');
                }
                
                redirect('franchiseListing');
            }
        }
    }

      function editOldFranchise($frachiseId = NULL)
    {
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {
            if($frachiseId == null)
            {
                redirect('franchiseListing');
            }
            
            $data['states'] = $this->Franchise_model->getCitylist();
            $data['franchiseInfo'] = $this->Franchise_model->getFranchiseInfo($frachiseId);
            
            $this->global['pageTitle'] = 'Intellikidz : Edit Franchise';
            
            $this->loadViews("editOldFranchise", $this->global, $data, NULL);
        }
    }

    function editFranchise()
    {
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {
            $this->load->library('form_validation');
            
            $this->form_validation->set_rules('cname','Centre Name','trim|required|max_length[128]|xss_clean');
            $this->form_validation->set_rules('coordinator','coordinator','trim|required|xss_clean|max_length[128]');
            $this->form_validation->set_rules('email','email','required|max_length[40]|xss_clean');
            $this->form_validation->set_rules('dob','dob','trim|required|max_length[30]|xss_clean');
            $this->form_validation->set_rules('mno','mno','trim|required|max_length[40]|xss_clean');
            $this->form_validation->set_rules('saddress','saddress','trim|required|max_length[250]|xss_clean');
            $this->form_validation->set_rules('state','state','trim|required|xss_clean');
            $this->form_validation->set_rules('cdate','cdate','trim|required|xss_clean');
            $this->form_validation->set_rules('cyear','cyear','trim|required|xss_clean');
            $this->form_validation->set_rules('rdate','rdate','trim|required|xss_clean');
            $this->form_validation->set_rules('btype','btype','trim|required|xss_clean');
            // $this->form_validation->set_rules('btype','sadress','trim|required|max_length[30]|xss_clean');
            
            if($this->form_validation->run() == FALSE)
            {
                // echo 'faild'; die;
                $this->editOldFranchise();
            }
            else
            {
                // print_r($_POST);    
                // var_dump($_FILES);
                // die;
                $franchiseId = $this->input->post('id');
                $franchise_id = $this->input->post('franchise_id');
                $file_name = '';
                if(isset($_FILES['image']) && $_FILES['image']['size']>0){
                    // echo 'ii fdf';
                   // print_r($_FILES);
                //    print_r($_POST); die;
                    $errors= array();
                    $file_name = $_FILES['image']['name'];
                    $file_size = $_FILES['image']['size'];
                    $file_tmp = $_FILES['image']['tmp_name'];
                    $file_type = $_FILES['image']['type'];
                    $file_ext=explode('.',$file_name);
                    $file_ext1=strtolower(end($file_ext));

                    $expensions= array("jpeg","jpg","png","PNG");

                    if(in_array($file_ext1,$expensions)=== false){
                        $errors[]="extension not allowed, please choose a JPEG or PNG file.";
                    }

                    if($file_size > 2097152) {
                        $errors[]='File size must be excately 2 MB';
                    }

                    if(empty($errors)==true) {
                        move_uploaded_file($file_tmp,"assets/images/franchise/".$file_name);
                        // echo "Success";
                    }else{
                        print_r($errors);
                    }
                }

                // die;
                $cname = $this->input->post('cname');
                $coordinator = $this->input->post('coordinator');
                $email = $this->input->post('email');
                $dob = $this->input->post('dob');
                $mobileno = $this->input->post('mno');
                $streetAddress = $this->input->post('saddress');
                $city = $this->input->post('state');
                $commemceDate =  $this->input->post('cdate');
                $commenceYear = $this->input->post('cyear');
                $renewalDate = $this->input->post('rdate');
                $branchType = $this->input->post('btype');
                // $profilePic = $file_name;

                // echo $file_name.'is photo';
                // $state = 'KA01';
                // echo $dob;
                // $myDateTime = DateTime::createFromFormat('Y-m-d', $dob);
                // $val = $myDateTime->format('Y-m-d');
                // echo $val;

                $dob = new DateTime($dob);
                $commemceDate = new DateTime($commemceDate);
                $renewalDate = new DateTime($renewalDate);

                // echo $date->format('d.m.Y'); // 31.07.2012
                // echo $date->format('Y-m-d');
                
                
                $franchiseInfo = array('franchise_name'=>$cname,
                'city_id'=> $city, 
                'contact_person_name' =>$coordinator,
                'contact_person_dob' => $dob->format('Y-m-d'),
                'contact_person_email' => $email, 
                'franchise_address' => $streetAddress, 
                'contact_person_mobile' => $mobileno,
                'franchise_commence_date' => $commemceDate->format('Y-m-d'),
                // 'contact_commenct' => $commenceYear,
                'franchise_renewal_date' => $renewalDate->format('Y-m-d'));

                if(!empty($file_name)){
                     $franchiseInfo['photo'] = $file_name;
                }

                // print_r($franchiseInfo); die;
                // contact_person_dob
                // franchise_info
                // franchise_code
                // franchise_commence_date
                // franchise_renewal_date
                // franchise_type_id
                // photo
                // status
                
                $this->load->model('Franchise_model');
                $result = $this->Franchise_model->editFranchise($franchiseInfo,$franchiseId,$franchise_id);

                
                if($result > 0)
                {
                    $this->session->set_flashdata('success', 'Franchise updated successfully');
                }
                else
                {
                    $this->session->set_flashdata('error', 'Franchise upation failed');
                }
                
                redirect('franchiseListing');
            }
        }
    }
    
    function pageNotFound()
    {
        $this->global['pageTitle'] = 'Intellikidz : 404 - Page Not Found';
        
        $this->loadViews("404", $this->global, NULL, NULL);
    }
}

?>