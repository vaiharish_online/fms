<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

/**
 * Class : User (UserController)
 * User Class to control all user related operations.
 * @author : Kishor Mali
 * @version : 1.1
 * @since : 15 November 2016
 */
class City extends BaseController
{
    /**
     * This is default constructor of the class
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->model('city_model');
        $this->isLoggedIn();   
    }
    
    /**
     * This function used to load the first screen of the user
     */
    public function index()
    {
        $this->global['pageTitle'] = 'Inellikidz : City';
        
        $this->loadViews("city", $this->global, NULL , NULL);
    }

    /**
     * This function is used to load the user list
     */
    function cityListing()
    {
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {
            $this->load->model('city_model');
        
            // $searchText = $this->input->post('searchText');
            // $data['searchText'] = $searchText;
            
            //$this->load->library('pagination');
            
            // $count = $this->city_model->cityListingCount($searchText);

            // $returns = $this->paginationCompress ( "cityListing/", $count, 5);
            
            // print_r($count);  die;
            
            //$data['cityRecords'] = $this->city_model->cityListing($searchText, $returns["page"], $returns["segment"]);
            $data['cityRecords'] = $this->city_model->cityListing();
            // print_r($data); die;
            $this->global['pageTitle'] = 'Intellikidz : city Listing';
            
            $this->loadViews("city", $this->global, $data, NULL);
        }
    }

    /**
     * This function is used to load the add new form
     */
    function addNewCity()
    {
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {
            $this->load->model('city_model');
            $data['states'] = $this->city_model->getStatelist();
            
            $this->global['pageTitle'] = 'Intellikidz : Add New City';

            $this->loadViews("addNewCity", $this->global, $data, NULL);
        }
    }
    
    /**
     * This function is used to add new user to the system
     */
    function addNewCity1()
    {
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {
            $this->load->library('form_validation');
            
            $this->form_validation->set_rules('cname','City Name','trim|required|max_length[128]|xss_clean');
            $this->form_validation->set_rules('district','District','trim|required|xss_clean|max_length[128]');
            $this->form_validation->set_rules('code','Code','required|max_length[40]|xss_clean');
            $this->form_validation->set_rules('pincode','pincode','trim|required|max_length[30]|xss_clean');
            $this->form_validation->set_rules('state','state','trim|required');
            
            if($this->form_validation->run() == FALSE)
            {
                // echo 'faild'; die;
                $this->addNewCity();
            }
            else
            {
                // print_r($_POST);
                // echo 'here'; die;
                $cname = $this->input->post('cname');
                $district = $this->input->post('district');
                $code = $this->input->post('code');
                $pincode = $this->input->post('pincode');
                $state = $this->input->post('state');
                // $state = 'KA01';
                
                $cityInfo = array('city_name'=>$cname, 'city_id'=> 'BA1', 'district_name'=>$district, 'state_id'=>$state, 'pincode'=> $pincode,'city_code'=>$code);
                
                $this->load->model('city_model');
                $result = $this->city_model->addNewCity($cityInfo);
                
                if($result > 0)
                {
                    $this->session->set_flashdata('success', 'New City created successfully');
                }
                else
                {
                    $this->session->set_flashdata('error', 'City creation failed');
                }
                
                redirect('cityListing');
            }
        }
    }

    
    /**
     * This function is used load user edit information
     * @param number $userId : Optional : This is user id
     */
    function editOldCity($cityId = NULL)
    {
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {
            if($cityId == null)
            {
                redirect('cityListing');
            }
            
            $data['states'] = $this->city_model->getStatelist();
            $data['cityInfo'] = $this->city_model->getCityInfo($cityId);
            
            $this->global['pageTitle'] = 'Intellikidz : Edit User';
            
            $this->loadViews("editOldCity", $this->global, $data, NULL);
        }
    }
    
    
    /**
     * This function is used to edit the user information
     */
    function editCity()
    {
        if($this->isAdmin() == TRUE)
        {
            $this->loadThis();
        }
        else
        {
            $this->load->library('form_validation');
            
            $cityId = $this->input->post('id');
            
            $this->form_validation->set_rules('cname','City Name','trim|required|max_length[128]|xss_clean');
            $this->form_validation->set_rules('district','District','trim|required|xss_clean|max_length[128]');
            $this->form_validation->set_rules('code','Code','required|max_length[40]|xss_clean');
            $this->form_validation->set_rules('pincode','Pincode','trim|required|max_length[30]|xss_clean');
            $this->form_validation->set_rules('state','state','trim|required');
            
            if($this->form_validation->run() == FALSE)
            {
                $this->editOldCity($cityId);
            }
            else
            {
                
                $cname = $this->input->post('cname');
                $district = $this->input->post('district');
                $code = $this->input->post('code');
                $pincode = $this->input->post('pincode');
                $state = $this->input->post('state');
                // $state = 'KA01';
                
                $cityInfo = array('city_name'=>$cname, 'city_id'=> 'BA1', 'district_name'=>$district, 'state_id'=>$state, 'pincode'=> $pincode,'city_code'=>$code);
                
                $this->load->model('city_model');
                
                $result = $this->city_model->editCity($cityInfo, $cityId);
                
                if($result == true)
                {
                    $this->session->set_flashdata('success', 'City updated successfully');
                }
                else
                {
                    $this->session->set_flashdata('error', 'City updation failed');
                }
                
                redirect('cityListing');
            }
        }
    }

    /**
    * This function is used to delete the user using userId
    * @return boolean $result : TRUE / FALSE
    */
    function deleteCity()
    {
        if($this->isAdmin() == TRUE)
        {
            echo(json_encode(array('status'=>'access')));
        }
        else
        {
            $cityId = $this->input->post('cityid');
            $cityInfo = array('status'=>0,'updated_date'=>date('Y-m-d H:i:s'));
            
            $result = $this->city_model->deleteCity($cityId, $cityInfo);
            
            if ($result > 0) { echo(json_encode(array('status'=>TRUE))); }
            else { echo(json_encode(array('status'=>FALSE))); }
        }
    }

    function pageNotFound()
    {
        $this->global['pageTitle'] = 'Intellikidz : 404 - Page Not Found';
        
        $this->loadViews("404", $this->global, NULL, NULL);
    }

}

?>