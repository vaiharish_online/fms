<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <i class="fa fa-users"></i> Franchise Management
        <small>Add, Edit, Delete</small>
      </h1>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12 text-right">
                <div class="form-group">
                    <a class="btn btn-primary" href="<?php echo base_url(); ?>addNewFranchise"><i class="fa fa-plus"></i> Add New Franchise</a>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
              <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Franchise List</h3>
                    <div class="box-tools">
                        <form action="<?php echo base_url() ?>franchiseListing" method="POST" id="searchList">
                           <!--  <div class="input-group">
                              <input type="text" name="searchText" value="" class="form-control input-sm pull-right" style="width: 150px;" placeholder="Search"/>
                              <div class="input-group-btn">
                                <button class="btn btn-sm btn-default searchList"><i class="fa fa-search"></i></button>
                              </div>
                            </div> -->
                        </form>
                    </div>
                </div><!-- /.box-header -->
                <div class="box-body table-responsive no-padding">
                  <table class="table table-hover" id="dataTable">
                    <thead>
                      <tr>
                        <th>#</th>
                        <th>Photo</th>
                        <th>BranchName & Address</th>
                        <th>Branch Details</th>
                        <th>Co-ordinator</th>
                        <th>Status</th>
                        <th class="text-center">Actions</th>
                      </tr>
                    </thead>
                    <?php foreach($franchiseRecords as  $key => $record): ?>
                    <tr>
                      <td><?= ($key+1); ?></td>
                      <td><img src="<?php echo base_url().'assets/images/franchise/'.$record->photo; ?>" style="width:60px; height:60px;" class="img-circle"></td>
                        <?php base64_encode($record->photo); ?>
                      <td>
                        <span><b><?php echo $record->franchise_name ?></b></span><br>
                        <span><?php echo $record->franchise_address ?></span>
                      </td>

                      <td>
                         <span><label>Branch Code:</label><span style="color: red;"><?php echo ' '.$record->franchise_id?></span><br>
                        <span><label>Commence Date:</label><?php $date=date_create($record->franchise_commence_date); echo ' '.date_format($date,'d-m-Y');?></span><br>
                        <span><label>Renewal Date:</label><?php $date=date_create($record->franchise_renewal_date); echo ' '.date_format($date,'d-m-Y');?></span>                          
                      </td>

                      <td>
                         <span><label>Name: </label><?= ' '.$record->contact_person_name?></span><br>
                        <span><label>DOB:</label><?php $date=date_create($record->contact_person_dob); echo ' '.date_format($date,'d-m-Y'); ?></span><br>
                        <span><label>ContactNo: </label><?php echo ' '.$record->contact_person_mobile ?></span><br>
                        <span><label>Email: </label><?php echo ' '.$record->contact_person_email ?></span>
                      </td>
                      <td><?php  if($record->status==1){echo 'Active';}else{echo 'Inactive';} ?></td>
                      <td class="text-center">
                          <a class="btn btn-sm btn-info" href="<?php echo base_url().'editOldFranchise/'.$record->id; ?>"><i class="fa fa-pencil"></i></a>
                          <!-- <a class="btn btn-sm btn-danger deleteUser" href="#" data-userid="<?php echo $record->userId; ?>"><i class="fa fa-trash"></i></a> -->
                      <!-- </td> -->
                    </tr>
                  <?php endforeach; ?>  
                  </table>
                  
                </div><!-- /.box-body -->
                <div class="box-footer clearfix">
                    
                </div>
              </div><!-- /.box -->
            </div>
        </div>
    </section>
</div>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/common.js" charset="utf-8"></script>
<!-- <script type="text/javascript">
    jQuery(document).ready(function(){
        jQuery('ul.pagination li a').click(function (e) {
            e.preventDefault();            
            var link = jQuery(this).get(0).href;            
            var value = link.substring(link.lastIndexOf('/') + 1);
            jQuery("#searchList").attr("action", baseURL + "franchiseListing/" + value);
            jQuery("#searchList").submit();
        });
    });
</script> -->
