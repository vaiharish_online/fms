<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <i class="ion ion-location fa-lg"></i> Franchise Registration
        <small>Add / Edit Franchise</small>
      </h1>
    </section>
    
    <section class="content">
    
        <div class="row">
            <!-- left column -->
            <div class="col-md-8">
              <!-- general form elements -->
                
                
                
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Enter Franchise Details</h3>
                    </div><!-- /.box-header -->
                    <!-- form start -->
                    
                    <form role="form" id="addFranchise" action="<?php echo base_url() ?>addNewFranchise1" method="post" enctype="multipart/form-data" role="form">
                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-6">                                
                                    <div class="form-group">
                                        <label for="cname">Centre Name</label>
                                        <input type="text" class="form-control required" id="cname" name="cname" maxlength="128">
                                    </div>
                                    
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="coordinator">Co-ordinator</label>
                                        <input type="text" class="form-control required" id="coordinator"  name="coordinator" maxlength="128">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="email">Email</label>
                                        <input type="text" class="form-control required" id="email"  name="email" maxlength="40">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="dob">DOB</label>
                                        <input type="text" class="form-control required" id="dob" name="dob" maxlength="30">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="mno">Mobile No</label>
                                        <input type="text" class="form-control required" id="mno"  name="mno" maxlength="40">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="dob">Street Address</label>
                                        <!-- <input type="test" class="form-control required" id="saddress" name="saddress" maxlength="30"> -->
                                        <textarea class="form-control required" id="saddress" name="saddress" maxlength="250"></textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="row">   
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="state">City Name</label>
                                        <select class="form-control required" id="state" name="state">
                                            <option value="0">Select City</option>
                                            <?php
                                            if(!empty($states))
                                            {
                                                foreach ($states as $rl)
                                                {
                                                    ?>
                                                    <option value="<?php echo $rl->id ?>"><?php echo $rl->city_name ?></option>
                                                    <?php
                                                }
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>   
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="cdate">Commence Date</label>
                                        <input type="cdate" class="form-control required" id="cdate" name="cdate" maxlength="30">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="cyear">Commence Year</label>
                                        <input type="cyear" class="form-control required" id="cyear"  name="cyear" maxlength="40">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="rdate">Renewal Date</label>
                                        <input type="rdate" class="form-control required" id="rdate" name="rdate" maxlength="30">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="btype">Branch Type</label>
                                        <select id="btype" class="form-control required" name="btype">
                                            <option value="0">Select Branchtype</option>
                                            <option value="Franchise">Franchise</option>
                                            <option value="School">School</option>
                                        </select> 
                                       <!--  <label for="btype">Branch Type</label>
                                        <input type="btype" class="form-control required" id="btype"  name="btype" maxlength="40"> -->
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="img">Uplod Photo</label>
                                        <input type="file" class="form-control required" id="image"  name="image" maxlength="40">
                                    </div>
                                </div>
                            </div>
                        </div><!-- /.box-body -->
    
                        <div class="box-footer">
                            <input type="submit" class="btn btn-primary" value="Submit" />
                            <input type="reset" class="btn btn-default" value="Reset" />
                            <input type="button" class="btn btn-default" onClick="document.location.href='franchiseListing'" value="Cancel"/>
                        </div>
                    </form>
                </div>
            </div>
            <div class="col-md-4">
                <?php
                    $this->load->helper('form');
                    $error = $this->session->flashdata('error');
                    if($error)
                    {
                ?>
                <div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <?php echo $this->session->flashdata('error'); ?>                    
                </div>
                <?php } ?>
                <?php  
                    $success = $this->session->flashdata('success');
                    if($success)
                    {
                ?>
                <div class="alert alert-success alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <?php echo $this->session->flashdata('success'); ?>
                </div>
                <?php } ?>
                
                <div class="row">
                    <div class="col-md-12">
                        <?php echo validation_errors('<div class="alert alert-danger alert-dismissable">', ' <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></div>'); ?>
                    </div>
                </div>
            </div>
        </div>    
    </section>
    
</div>
<script type="text/javascript">
    $( function() {
        $( "#dob" ).datepicker();
        $( "#cdate" ).datepicker();
        $( "#rdate" ).datepicker();
    } );
</script>
<script src="<?php echo base_url(); ?>assets/js/addFranchise.js" type="text/javascript"></script>