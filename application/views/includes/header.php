<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <title><?php echo $pageTitle; ?></title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <!-- Bootstrap 3.3.4 -->
    <link href="<?php echo base_url(); ?>assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />    
    <!-- FontAwesome 4.3.0 -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <!-- Ionicons 2.0.0 -->
    <!-- <link href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css" rel="stylesheet" type="text/css" /> -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/ionicons/css/ionicons.min.css" type="text/css"/>
    <!-- Theme style -->
    <link href="<?php echo base_url(); ?>assets/dist/css/AdminLTE.css" rel="stylesheet" type="text/css" />
    <!-- AdminLTE Skins. Choose a skin from the css/skins 
         folder instead of downloading all of them to reduce the load. -->
    <link href="<?php echo base_url(); ?>assets/dist/css/skins/_all-skins.min.css" rel="stylesheet" type="text/css" />
   
<!-- p & c enable below script -->
    <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.6.1/css/buttons.dataTables.min.css">
     <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css">

    <style>
    	.error{
    		color:red;
    		font-weight: normal;
    	}
.badge {
  position: absolute;
  right: 10px;
  /*padding: 5px 10px;*/
  /*border-radius: 40%;*/
  /*background: red;*/
  color: white;
}
    </style>
    <!-- jQuery 2.1.4 -->
    <script src="<?php echo base_url(); ?>assets/js/jQuery-2.1.4.min.js"></script>
    <script type="text/javascript">
        var baseURL = "<?php echo base_url(); ?>";
    </script>
    
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
   <?php
    if($role == ROLE_ADMIN)
    {
    ?>
      <body class="skin-blue sidebar-mini">
    <?php }else if($role == ROLE_MANAGER || $role == ROLE_FRANCHISE)
    { ?>
      <body class="skin-green sidebar-mini">
  <?php } ?>
  <!-- <body class="skin-blue sidebar-mini"> -->
    <div class="wrapper">
      
      <header class="main-header">
        <!-- Logo -->
        <a href="<?php echo base_url(); ?>" class="logo">
          <!-- mini logo for sidebar mini 50x50 pixels -->
          <span class="logo-mini"><b>INTL</b>KIDS</span>
          <!-- logo for regular state and mobile devices -->
          <!-- <span class="logo-lg"><b>CodeInsect</b>AS</span> -->
          <span class="logo-lg"><b>INTELLIKIDZ</b></span>
        </a>
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top" role="navigation">
          <!-- Sidebar toggle button-->
          <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
          </a>
          <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
              <!-- User Account: style can be found in dropdown.less -->
              <li class="dropdown user user-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  <!-- <img src="<?php echo base_url(); ?>assets/dist/img/avatar.png" class="user-image" alt="User Image"/> -->
                  <img src="<?php echo base_url().'assets/images/franchise/'. $photo; ?>" class="user-image" alt="User Image"/>
                  <span class="hidden-xs"><?php echo $name; ?></span>
                </a>
                <ul class="dropdown-menu">
                  <!-- User image -->
                  <li class="user-header">
                    <img src="<?php echo base_url().'assets/images/franchise/'. $photo; ?>" class="img-circle" alt="User Image" />
                    <p>
                      <?php echo $name; ?>
                      <small><?php echo $role_text; ?></small>
                    </p>
                  </li>
                  <!-- Menu Footer-->
                  <li class="user-footer">
                    <div class="pull-left">
                      <a href="<?php echo base_url(); ?>loadChangePass" class="btn btn-default btn-flat"><i class="fa fa-key"></i> Change Password</a>
                    </div>
                    <div class="pull-right">
                      <a href="<?php echo base_url(); ?>logout" class="btn btn-default btn-flat"><i class="fa fa-sign-out"></i> Sign out</a>
                    </div>
                  </li>
                </ul>
              </li>
            </ul>
          </div>
        </nav>
      </header>
      <!-- Left side column. contains the logo and sidebar -->
      <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
          <div class="user-panel">
        <div class="pull-left image">
          <img src="<?php echo base_url().'assets/images/franchise/'. $photo; ?>" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p><?php echo $name; ?></p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>
      <!-- search form -->
      <form action="#" method="get" class="sidebar-form">
        <div class="input-group">
          <input type="text" name="q" class="form-control" placeholder="Search...">
              <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat">
                  <i class="fa fa-search"></i>
                </button>
              </span>
        </div>
      </form>
          <!-- sidebar menu: : style can be found in sidebar.less -->
          <ul class="sidebar-menu">
            <li class="header">MAIN NAVIGATION</li>
            <li class="treeview">
              <a href="<?php echo base_url(); ?>dashboard">
                <i class="fa fa-dashboard"></i> <span>Dashboard</span></i>
              </a>
            </li>
           <!-- <li class="treeview">
              <a href="#" >
                <i class="fa fa-plane"></i>
                <span>Franchise</span>
              </a>
            </li>
            <li class="treeview">
              <a href="#" >
                <i class="fa fa-ticket"></i>
                <span>Admission Details</span>
              </a>
            </li> -->
            <?php
            if($role == ROLE_ADMIN)
            {
            ?>
            <!-- <li class="treeview">
              <a href="#" >
                <i class="fa fa-thumb-tack"></i>
                <span>Educere Details</span>
              </a>
            </li> -->
              <li class="treeview">
              <a href="<?php echo base_url(); ?>franchiseListing">
                <i class="fa fa-users"></i>
                <span>Franchise</span><span class="badge"><?php echo $franchiseCount;  ?></span>
              </a>
            </li>
             <li class="treeview">
              <a href="#">
                <i class="fa fa-ticket"></i>
                <span>Admission Details</span><span class="badge">24900</span>
              </a>
            </li>
             <li class="treeview">
             <a href="<?php echo base_url(); ?>educereListing">
                <i class="fa fa-users"></i>
                <span>Educere Details</span><span class="badge">15</span>
              </a>
            </li>
            <li class="treeview">
              <a href="<?php echo base_url(); ?>cityListing">
                <i class="ion ion-location"></i>
                <span>City Details</span><span class="badge"><?php echo $cityCounts;  ?></span>
              </a>
            </li>
             <li class="treeview">
              <a href="#">
                <i class="fa fa-users"></i>
                <span>Competions Details</span>
              </a>
            </li>
             <li class="treeview">
              <a href="#">
                <i class="fa fa-rss-square"></i>
                <span>News</span>
              </a>
            </li>
             <li class="treeview">
              <a href="#">
                <i class="fa fa-users"></i>
                <span>Self Assesment</span>
              </a>
            </li>
             <li class="treeview">
              <a href="#">
                <i class="fa fa-exchange"></i>
                <span>Transfer</span>
              </a>
            </li>
             <li class="treeview">
              <a href="#">
                <i class="fa fa-ticket"></i>
                <span>Hall Ticket</span>
              </a>
            </li>
             
            <?php
            }
            if($role == ROLE_ADMIN)
            {
            ?>
        <!--  <li class="treeview">
              <a href="<?php echo base_url(); ?>userListing">
                <i class="fa fa-users"></i>
                <span>Users</span>
              </a>
            </li> -->
          <!--  <li class="treeview">
              <a href="#" >
                <i class="fa fa-files-o"></i>
                <span>Reports</span>
              </a>
            </li> -->
            <li class="treeview">
              <a href="#">
                <i class="fa fa-graduation-cap"></i>
                <span>Graduates</span>
              </a>
            </li>
            <?php
            }
            ?>

            <?php if($role == ROLE_MANAGER || $role == ROLE_FRANCHISE)
            {
            ?>
             <li class="treeview">
              <a href="#">
                <i class="fa fa-users"></i>
                <span>Franchise</span>
              </a>
            </li>
             <li class="treeview">
              <a href="#">
                <i class="fa fa-ticket"></i>
                <span>Admission Details</span><span class="badge">150</span>
              </a>
            </li>
            <li class="treeview">
              <a href="#">
                <i class="fa fa-ticket"></i>
                <span>New Admission</span>
              </a>
            </li>
            <li class="treeview">
              <a href="#">
                <i class="ion ion-location"></i>
                <span>Competition Details</span><span class="badge">50</span>
              </a>
            </li>
             <li class="treeview">
              <a href="#">
                <i class="fa fa-users"></i>
                <span>Self Assesment</span>
              </a>
            </li>
             <li class="treeview">
              <a href="#">
                <i class="fa fa-rss-square"></i>
                <span>News</span>
              </a>
            </li>
            <?php
            }
            ?>

          </ul>
        </section>
        <!-- /.sidebar -->
      </aside>