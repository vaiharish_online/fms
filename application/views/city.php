
  <!-- <link rel="stylesheet" type="text/css"  href="<?php echo base_url(); ?>assets/plugins/datatables/extensions/AutoFill/css/dataTables.autoFill.css">
  <link rel="stylesheet" type="text/css"  href="<?php echo base_url(); ?>assets/plugins/datatables/jquery.dataTables.css"> -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <i class="io ion-location fa-lg"></i> City Details
        <small>Add, Edit, Delete</small>
      </h1>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12 text-right">
                <div class="form-group">
                    <a class="btn btn-primary" href="<?php echo site_url(); ?>addNewCity"><i class="fa fa-plus"></i> Add New</a>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
              <div class="box">
                <div class="box-header">
                    <h3 class="box-title">City List</h3>
                    <div class="box-tools">
                        <form action="<?php echo base_url() ?>cityListing" method="POST" id="searchList">
                            <!-- <div class="input-group">
                             <input type="text" name="searchText" value="" class="form-control input-sm pull-right" style="width: 150px;" placeholder="Search"/> 
                              <div class="input-group-btn">
                                <button class="btn btn-sm btn-default searchList"><i class="fa fa-search"></i></button>
                              </div>
                            </div> -->
                        </form>
                    </div>
                </div><!-- /.box-header -->
                <div class="box-body table-responsive no-padding">
                  <table class="table table-hover" id="dataTable">
                  <thead>
                    <tr>
                      <th>#</th>
                      <th>City Name</th>
                      <th>State</th>
                      <th>Code</th>
                      <th>Pincode</th>
                      <th>District</th>
                      <th class="text-center">Actions</th>
                    </tr>
                  </thead>
                  <?php
                //   print_r($cityRecords); die;
                    if(!empty($cityRecords))
                    {
                      $i=1;
                        foreach($cityRecords as $record)
                        {
                    ?>
                    <tr>
                      <td><?=  $i ?></td>
                      <td><?php echo $record->city_name ?></td>
                      <td><?php echo $record->state_name ?></td>
                      <td><?php echo $record->city_code ?></td>
                      <td><?php echo $record->pincode ?></td>
                      <td><?php echo $record->district_name ?></td>
                      <td class="text-center">
                          <a class="btn btn-sm btn-info" href="<?php echo base_url().'editOldCity/'.$record->id; ?>"><i class="fa fa-pencil"></i></a>
                          <a class="btn btn-sm btn-danger deleteCity" href="#" data-cityId="<?php echo $record->id; ?>"><i class="fa fa-trash"></i></a>
                      </td>
                    </tr>
                    <?php
                    $i++;
                        }
                    }
                    ?>
                  </table>
                </div><!-- /.box-body -->
                <div class="box-footer clearfix">
                
                </div>
              </div><!-- /.box -->
            </div>
        </div>
    </section>
</div>

<!-- <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/common.js" charset="utf-8"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/plugins/datatables/jquery.dataTables.js"></script>
  <script type="text/javascript" src="<?php echo base_url(); ?>assets/plugins/datatables/extensions/AutoFill/js/dataTables.autoFill.js"></script>
  <script type="text/javascript" src="<?php echo base_url(); ?>assets/plugins/datatables/jquery.dataTables.min.js"></script>
  <script type="text/javascript" src="<?php echo base_url(); ?>assets/plugins/datatables/dataTables.bootstrap.min.js"></script> -->
<script type="text/javascript">
    jQuery(document).ready(function(){
    //     var table = $('#table1').dataTable({
    //       "lengthMenu": [[5, 10, 15, -1], [5, 10, 15, "All"]]
    //     });
    //     new $.fn.dataTable.AutoFill( table, {
    //       "columnDefs": [
    //         { targets: [0, 1], visible: true},
    //         { targets: '_all', visible: false }
    //       ]
    //     });
        
    //   $('#table1').on( 'page.dt', function (ev) {
    //   // var info = table.page.info();
    //   console.log(ev);
    //   } ).DataTable();

    // $('#table1 tbody').on('click', 'tr', function () {
    //     var data = table.row( this ).data();
    //     alert( 'You clicked on '+data[0]+'\'s row' );
    // } );
          
        // jQuery('ul.pagination li a').click(function (e) {
        //     e.preventDefault();            
        //     var link = jQuery(this).get(0).href;            
        //     var value = link.substring(link.lastIndexOf('/') + 1);
        //     jQuery("#searchList").attr("action", baseURL + "cityListing/" + value);
        //     jQuery("#searchList").submit();
        // });
    });
</script>
