<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <i class="fa fa-tachometer" aria-hidden="true"></i> Dashboard
        <small>Control panel</small>
      </h1>
    </section>
    
    <section class="content">
        <div class="row">
           <?php
            $addmisstion = 150;
            if($role == ROLE_ADMIN)
            {
              $addmisstion = 24900;
            ?>          
              <div class="col-lg-3 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-green">
                  <div class="inner">
                    <h3>15<sup style="font-size: 20px"></sup></h3>
                    <p>Educere Details</p>
                  </div>
                  <div class="icon">
                   <!-- <i class="ion ion-stats-bars"></i> -->
                  </div>
                  <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                </div>
              </div><!-- ./col -->
           
            <div class="col-lg-3 col-xs-6">
              <!-- small box -->
              <div class="small-box bg-yellow">
                <div class="inner">
                  <h3><?php echo $Usercount ?></h3>
                  <p>Franchises</p>
                </div>
                <div class="icon">
                  <i class="ion ion-person-add"></i>
                </div>
                <a href="<?php echo base_url(); ?>franchiseListing" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
              </div>
            </div><!-- ./col -->
            <?php 
              } 
            ?>
         
          <?php if($role == ROLE_MANAGER || $role == ROLE_FRANCHISE)
          {
          ?>
            <div class="col-lg-3 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-green">
                  <div class="inner">
                    <h3>50<sup style="font-size: 20px"></sup></h3>
                    <p>Competition Details</p>
                  </div>
                  <div class="icon">
                   <!-- <i class="ion ion-stats-bars"></i> -->
                  </div>
                  <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                </div>
              </div>
              <div class="col-lg-3 col-xs-6">
              <!-- small box -->
              <div class="small-box bg-yellow">
                <div class="inner">
                  <h3><?php echo $Usercount ?></h3>
                  <p>Certificates issued</p>
                </div>
                <div class="icon">
                  <i class="ion ion-certificate"></i>
                </div>
                <a href="<?php echo base_url(); ?>userListing" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
              </div>
            </div><!-- ./col -->
          <?php } ?>
          <div class="col-lg-3 col-xs-6">
              <!-- small box -->
              <div class="small-box bg-red">
                <div class="inner">
                  <h3><?php echo $addmisstion ?></h3>
                  <p>Total Adminssions</p>
                </div>
                <div class="icon">
                  <!-- <i class="ion ion-pie-graph"></i> -->
                </div>
                <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
              </div>
            </div><!-- ./col -->
            <?php if($role == ROLE_ADMIN)
            {
            ?>
              <div class="col-lg-3 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-aqua">
                  <div class="inner">
                    <h3><?php echo $Citycount ?></h3>
                    <p>Cities</p>
                  </div>
                  <div class="icon">
                    <i class="ion ion-location fa-lg"></i>
                  </div>
                  <a href="<?php echo base_url(); ?>cityListing" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                </div>
              </div><!-- ./col -->
            <?php } ?>
          </div>           
    </section>
</div>