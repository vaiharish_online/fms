<?php

// $userId = '';
// $name = '';
// $email = '';
// $mobile = '';
// $roleId = '';


 // print_r($franchiseInfo);

if(!empty($franchiseInfo))
{

    foreach ($franchiseInfo as $uf)
    {
        $id = $uf->id;
        $franchise_id = $uf->franchise_id;
        $cname = $uf->franchise_name;
        $coordinator = $uf->contact_person_name;
        $saddress = $uf->franchise_address;
        $dob = $uf->contact_person_dob;
        $city_id = $uf->city_id;
        $mno = $uf->contact_person_mobile;
        $email = $uf->contact_person_email;
        $cdate= $uf->franchise_commence_date ;
        $rdate = $uf->franchise_renewal_date;
        $cyear = $uf->franchise_renewal_date ;;
        $btype = 'Franchise';
        $photo = $uf->photo;

        // $district = $uf->district_name;
        // $code = $uf->city_code;
        // $pincode = $uf->pincode;
        // $state_id = $uf->state_id;

    }
}


?> 

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <i class="fa fa-users"></i> Franchise Management
        <small>Add / Edit Franchise</small>
      </h1>
    </section>
    
    <section class="content">
    
        <div class="row">
            <!-- left column -->
            <div class="col-md-8">
              <!-- general form elements -->
                
                
                
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Enter City Details</h3>
                    </div><!-- /.box-header -->
                    <!-- form start -->
                    
                    <form role="form" action="<?php echo base_url() ?>editFranchise" method="post" id="editFranchise" enctype="multipart/form-data" role="form">
                        <div class="box-body">
                           <div class="row">
                                <div class="col-md-6">                                
                                    <div class="form-group">
                                        <label for="cname">Centre Name</label>
                                        <input type="text" class="form-control required" id="cname" name="cname" value="<?php echo $cname; ?>" maxlength="128">
                                         <input type="hidden" value="<?php echo $id; ?>" name="id" id="id" /> 
                                          <input type="hidden" value="<?php echo $franchise_id; ?>" name="franchise_id" id="id" />    
                                    </div>
                                    
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="coordinator">Co-ordinator</label>
                                        <input type="text" class="form-control required" id="coordinator"  name="coordinator" value="<?php echo $coordinator; ?>" maxlength="128">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="email">Email</label>
                                        <input type="text" class="form-control required" id="email"  name="email" value="<?php echo $email; ?>" maxlength="40">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="dob">DOB</label>
                                        <input type="text" class="form-control required" id="dob" name="dob" value="<?php echo $dob; ?>" maxlength="30">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="mno">Mobile No</label>
                                        <input type="text" class="form-control required" id="mno"  name="mno" value="<?php echo $mno; ?>" maxlength="40">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="dob">Street Address</label>
                                        <!-- <input type="textarea" class="form-control required" id="saddress" name="saddress" maxlength="30"> -->
                                        <textarea  class="form-control required" id="saddress" name="saddress"  maxlength="250"><?php echo $saddress; ?></textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="row">   
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="state">City Name</label>
                                        <select class="form-control required" id="state" name="state">
                                            <option value="0">Select City</option>
                                            <?php
                                            if(!empty($states))
                                            {
                                                foreach ($states as $rl)
                                                {
                                                    ?>
                                                   <!--  <option value="<?php echo $rl->id ?>"><?php echo $rl->city_name ?></option> -->
                                                   <option value="<?php echo $rl->id; ?>" <?php if($rl->id == $city_id) {echo "selected=selected";} ?>><?php echo $rl->city_name ?></option>
                                                    <?php
                                                }
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>   
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="cdate">Commence Date</label>
                                        <input type="cdate" class="form-control required" id="cdate" name="cdate" value="<?php echo $cdate; ?>" maxlength="30">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="cyear">Commence Year</label>
                                        <input type="cyear" class="form-control required" id="cyear"  name="cyear" value="<?php echo $cyear; ?>" maxlength="40">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="rdate">Renewal Date</label>
                                        <input type="rdate" class="form-control required" id="rdate" name="rdate" value="<?php echo $rdate; ?>" maxlength="30">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="btype">Branch Type</label>
                                        <input type="btype" class="form-control required" id="btype"  name="btype" value="<?php echo $btype; ?>" maxlength="40">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="img">Edit Photo</label>
                                         <img src="<?php echo base_url().'assets/images/franchise/'.$photo; ?>" style="width:60px; height:60px;" class="img-circle">
                                        <input type="file" class="form-control" id="image"  name="image" value="<?php echo base_url().'assets/images/franchise/'.$photo; ?>" maxlength="40">
                                       
                                    </div>
                                </div>
                            </div>
                        </div><!-- /.box-body -->
    
                        <div class="box-footer">
                            <input type="submit" class="btn btn-primary" value="Submit" />
                            <input type="reset" class="btn btn-default" value="Reset" />
                             <!-- <input type="button" class="btn btn-default">'" value="Cancel"/> -->
                        </div>
                    </form>
                </div>
            </div>
            <div class="col-md-4">
                <?php
                    $this->load->helper('form');
                    $error = $this->session->flashdata('error');
                    if($error)
                    {
                ?>
                <div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <?php echo $this->session->flashdata('error'); ?>                    
                </div>
                <?php } ?>
                <?php  
                    $success = $this->session->flashdata('success');
                    if($success)
                    {
                ?>
                <div class="alert alert-success alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <?php echo $this->session->flashdata('success'); ?>
                </div>
                <?php } ?>
                
                <div class="row">
                    <div class="col-md-12">
                        <?php echo validation_errors('<div class="alert alert-danger alert-dismissable">', ' <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></div>'); ?>
                    </div>
                </div>
            </div>
        </div>    
    </section>
</div>

<!-- <script src="<?php echo base_url(); ?>assets/js/editCity.js" type="text/javascript"></script> -->
<script type="text/javascript">
    $( function() {
        $( "#dob" ).datepicker();
        $( "#cdate" ).datepicker();
        $( "#rdate" ).datepicker();
    } );
</script>
<script src="<?php echo base_url(); ?>assets/js/editFranchise.js" type="text/javascript"></script>