<?php

// $userId = '';
// $name = '';
// $email = '';
// $mobile = '';
// $roleId = '';


 print_r($educereInfo);

if(!empty($educereInfo))
{

    foreach ($educereInfo as $uf)
    {
        $id = $uf->id;
        $franchise_id = $uf->franchise_id;
        $fname = $uf->educere_name;
        $gender = $uf->gender;
        $address = $uf->address;
        $dob = $uf->dob;
        $bgroup = $uf->blood_group;
        $contactno = $uf->contact_number;
        $email = $uf->email;
        $pincode= $uf->pincode ;
        $qualification = $uf->qualification;
        $year = $uf->year;
        $btype = 'Franchise';
        $photo = $uf->photo;

        // $district = $uf->district_name;
        // $code = $uf->city_code;
        // $pincode = $uf->pincode;
        // $state_id = $uf->state_id;

    }
}


?> 

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <i class="fa fa-users"></i> Educere Management
        <small>Add / Edit Educere Details</small>
      </h1>
    </section>
    
    <section class="content">
    
        <div class="row">
            <!-- left column -->
            <div class="col-md-8">
              <!-- general form elements -->
                
                
                
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Enter Educere Details</h3>
                    </div><!-- /.box-header -->
                    <!-- form start -->
                    
                    <form role="form" action="<?php echo base_url() ?>editEducere" method="post" id="editEducere" enctype="multipart/form-data" role="form">
                        <div class="box-body">
                           <div class="row">
                                <div class="col-md-6">                                
                                    <div class="form-group">
                                        <label for="cname">FUll Name</label>
                                        <input type="text" class="form-control required" id="fname" name="fname" value="<?php echo $fname; ?>" maxlength="128">
                                         <input type="hidden" value="<?php echo $id; ?>" name="id" id="id" /> 
                                    </div>
                                    
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="coordinator">Franchise Name</label>
                                        <select class="form-control required" id="franchisename" name="franchisename">
                                            <option value="0">Select Frachise</option>
                                            <?php
                                            if(!empty($franchises))
                                            {
                                                foreach ($franchises as $rl)
                                                {
                                                    ?>
                                                   <option value="<?php echo $rl->id; ?>" <?php if($rl->id == $franchise_id) {echo "selected=selected";} ?>><?php echo $rl->franchise_name ?></option>
                                                    <?php
                                                }
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                    <label for="address">Address</label>
                                        <!-- <input type="test" class="form-control required" id="saddress" name="saddress" maxlength="30"> -->
                                        <textarea class="form-control required" id="address" name="address" maxlength="250"><?php echo $address ?></textarea>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="dob">DOB</label>
                                        <input type="text" class="form-control required" id="dob" value="<?php echo $dob ?>" name="dob" maxlength="30">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="cno">Contact No</label>
                                        <input type="text" class="form-control required" id="cno" value="<?php echo $contactno ?>" name="cno" maxlength="40">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="email">Email</label>
                                        <input type="text" class="form-control required" id="email"  name="email" value="<?php echo $email ?>" maxlength="40">
                                    </div>
                                </div>
                            </div>
                            <div class="row">   
                                <div class="col-md-6">
                                    <div class="form-group">
                                    <label for="pincode">Pincode</label>
                                        <input type="text" class="form-control required" id="pincode"  name="pincode" value="<?php echo $pincode ?>" maxlength="128">
                                    </div>
                                </div>   
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="bgroup">Blood Group</label>
                                        <input type="text" class="form-control required" id="bgroup" name="bgroup" value="<?php echo $bgroup ?>" maxlength="30">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="gender">Gender</label>
                                        <select id="gender" class="form-control required" name="gender" value="value="<?php echo $gender ?>">
                                            <option value="0">Select Gender</option>
                                            <option value="<?php echo $gender; ?>" <?php  {echo "selected=selected";} ?>><?php echo $gender ?></option>
                                            <option value="Male">Male</option>
                                            <option value="Female">Female</option>
                                        </select> 
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="qualification">Qualification</label>
                                        <input type="text" class="form-control required" id="qualification" name="qualification" value="<?php echo $qualification ?>" maxlength="30">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="year">Year</label>
                                        <select id="year" class="form-control required" name="year" value="<?php echo $year ?>">
                                            <option value="0">Select Year</option>
                                            <option value="<?php echo $year; ?>" <?php  {echo "selected=selected";} ?>><?php echo $year ?></option>
                                            <option value="2018">2018</option>
                                            <option value="2019">2019</option>
                                            <option value="2020">2020</option>
                                        </select> 
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="ldate">Level Date</label>
                                        <input type="text" class="form-control required" id="ldate" name="ldate" maxlength="30">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="alevel">Add Level</label>
                                        <select id="alevel" class="form-control required" name="alevel">
                                            <option value="0">Select Lelvel</option>
                                            <option value="IK-01">IK-01</option>
                                            <option value="IK-02">IK-02</option>
                                            <option value="IK-03">IK-03</option>
                                            <option value="IK-04">IK-04</option>
                                        </select> 
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="img">Uplod Photo</label>
                                        <img src="<?php echo base_url().'assets/images/franchise/'.$photo; ?>" style="width:60px; height:60px;" class="img-circle">
                                        <input type="file" class="form-control" id="image"  name="image" value="<?php echo base_url().'assets/images/franchise/'.$photo; ?>" maxlength="40">
                                    </div>
                                </div>
                            </div>
                        </div><!-- /.box-body -->
    
                        <div class="box-footer">
                            <input type="submit" class="btn btn-primary" value="Submit" />
                            <input type="reset" class="btn btn-default" value="Reset" />
                             <!-- <input type="button" class="btn btn-default">'" value="Cancel"/> -->
                        </div>
                    </form>
                </div>
            </div>
            <div class="col-md-4">
                <?php
                    $this->load->helper('form');
                    $error = $this->session->flashdata('error');
                    if($error)
                    {
                ?>
                <div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <?php echo $this->session->flashdata('error'); ?>                    
                </div>
                <?php } ?>
                <?php  
                    $success = $this->session->flashdata('success');
                    if($success)
                    {
                ?>
                <div class="alert alert-success alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <?php echo $this->session->flashdata('success'); ?>
                </div>
                <?php } ?>
                
                <div class="row">
                    <div class="col-md-12">
                        <?php echo validation_errors('<div class="alert alert-danger alert-dismissable">', ' <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></div>'); ?>
                    </div>
                </div>
            </div>
        </div>    
    </section>
</div>
<script src="<?php echo base_url(); ?>assets/js/editEducere.js" type="text/javascript"></script>
<script type="text/javascript">
    $( function() {
        $( "#dob" ).datepicker();
        $( "#ldate" ).datepicker();
    } );
</script>
