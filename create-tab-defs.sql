
CREATE TABLE `adm_franchise_master` (
 `id` int(11) NOT NULL AUTO_INCREMENT,
 `franchise_id` varchar(45) COLLATE utf8_bin NOT NULL,
 `franchise_name` varchar(100) COLLATE utf8_bin NOT NULL,
 `contact_person_name` varchar(100) COLLATE utf8_bin DEFAULT NULL,
 `contact_person_dob` date DEFAULT NULL,
 `contact_person_mobile` varchar(20) COLLATE utf8_bin DEFAULT NULL,
 `contact_person_email` varchar(100) COLLATE utf8_bin DEFAULT NULL,
 `franchise_address` text COLLATE utf8_bin DEFAULT NULL,
 `franchise_info` varchar(100) COLLATE utf8_bin DEFAULT NULL,
 `city_id` varchar(45) COLLATE utf8_bin NOT NULL,
 `franchise_code` varchar(45) COLLATE utf8_bin NOT NULL,
 `franchise_commence_date` date DEFAULT NULL,
 `franchise_renewal_date` date DEFAULT NULL,
 `franchise_type_id` varchar(45) COLLATE utf8_bin NOT NULL,
 `photo` varchar(1000) COLLATE utf8_bin NOT NULL,
 `status` tinyint(4) DEFAULT 1,
 `createdAt` datetime NOT NULL DEFAULT current_timestamp() COMMENT 'DC2Type:datetime',
 `updatedAt` datetime NOT NULL DEFAULT current_timestamp() COMMENT 'DC2Type:datetime',
 PRIMARY KEY (`id`,`franchise_id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 COLLATE=