-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 26, 2020 at 01:55 PM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.2.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `cias`
--

-- --------------------------------------------------------

--
-- Table structure for table `adm_city`
--

CREATE TABLE `adm_city` (
  `id` int(11) NOT NULL,
  `city_id` varchar(45) NOT NULL,
  `city_name` varchar(255) DEFAULT NULL,
  `city_code` varchar(45) NOT NULL,
  `pincode` varchar(45) NOT NULL,
  `district_name` varchar(255) NOT NULL,
  `state_id` varchar(45) NOT NULL,
  `status` tinyint(4) DEFAULT 1,
  `created_date` datetime DEFAULT current_timestamp(),
  `updated_date` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='this table is linked with adm_state table';

--
-- Dumping data for table `adm_city`
--

INSERT INTO `adm_city` (`id`, `city_id`, `city_name`, `city_code`, `pincode`, `district_name`, `state_id`, `status`, `created_date`, `updated_date`) VALUES
(1, '1', 'Bangalore', 'CA1', '5678', 'Bangalore1', 'KA01', 1, '2020-04-10 17:22:31', '2020-04-10 17:22:31'),
(2, 'BA2', 'Bangalore', 'B02', '560079', 'Bangaalore', 'KA01', 1, '2020-04-10 20:32:43', '2020-04-10 20:32:43'),
(3, '1', 'Mysore', 'MA2', '560066', 'Mysore', 'KA01', 1, '2020-04-10 20:35:03', '2020-04-10 20:35:03'),
(4, '1', 'Hubli', 'H13', '560066', 'Hubli', 'KA01', 1, '2020-04-10 20:36:31', '2020-04-10 20:36:31'),
(5, '1', 'Davangere', 'D34', '40076', 'Davangere', 'KA01', 0, '2020-04-10 21:08:47', '2020-04-11 16:12:42'),
(6, '1', 'Belgum', 'BL67', '560089', 'Chikkodi', 'KA01', 0, '2020-04-10 21:11:03', '2020-04-11 13:54:40'),
(7, '1', 'Tumkur', 'TM23', '5690', 'Gubbi', 'KA01', 0, '2020-04-10 21:32:46', '2020-04-11 13:53:03'),
(8, '1', 'Hassan', 'HS78', '67989', 'Belur', 'KA01', 0, '2020-04-10 21:39:30', '2020-04-11 13:52:32'),
(23, 'BA1', 'sf', '4545', '34343', 'sfs', 'KA01', 1, '2020-04-20 15:07:28', '2020-04-20 15:07:28');

-- --------------------------------------------------------

--
-- Table structure for table `adm_country`
--

CREATE TABLE `adm_country` (
  `id` int(11) NOT NULL,
  `country_id` varchar(45) NOT NULL,
  `country_code` varchar(45) NOT NULL,
  `country_name` varchar(255) DEFAULT NULL,
  `status` tinyint(4) DEFAULT 1,
  `created_date` datetime DEFAULT current_timestamp(),
  `updated_date` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `adm_country`
--

INSERT INTO `adm_country` (`id`, `country_id`, `country_code`, `country_name`, `status`, `created_date`, `updated_date`) VALUES
(1, 'IN', 'IN01', 'INDIA', 1, '2020-04-10 17:19:28', '2020-04-10 17:19:28');

-- --------------------------------------------------------

--
-- Table structure for table `adm_franchise_master`
--

CREATE TABLE `adm_franchise_master` (
  `id` int(11) NOT NULL,
  `franchise_id` varchar(45) COLLATE utf8_bin NOT NULL,
  `franchise_name` varchar(100) COLLATE utf8_bin NOT NULL,
  `contact_person_name` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `contact_person_dob` date DEFAULT NULL,
  `contact_person_mobile` varchar(20) COLLATE utf8_bin DEFAULT NULL,
  `contact_person_email` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `franchise_address` text COLLATE utf8_bin DEFAULT NULL,
  `franchise_info` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `city_id` varchar(45) COLLATE utf8_bin NOT NULL,
  `franchise_code` varchar(45) COLLATE utf8_bin NOT NULL,
  `franchise_commence_date` date DEFAULT NULL,
  `franchise_renewal_date` date DEFAULT NULL,
  `franchise_type_id` varchar(45) COLLATE utf8_bin NOT NULL,
  `photo` varchar(1000) COLLATE utf8_bin NOT NULL,
  `status` tinyint(4) DEFAULT 1,
  `createdAt` datetime NOT NULL DEFAULT current_timestamp() COMMENT 'DC2Type:datetime',
  `updatedAt` datetime NOT NULL DEFAULT current_timestamp() COMMENT 'DC2Type:datetime'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `adm_franchise_master`
--

INSERT INTO `adm_franchise_master` (`id`, `franchise_id`, `franchise_name`, `contact_person_name`, `contact_person_dob`, `contact_person_mobile`, `contact_person_email`, `franchise_address`, `franchise_info`, `city_id`, `franchise_code`, `franchise_commence_date`, `franchise_renewal_date`, `franchise_type_id`, `photo`, `status`, `createdAt`, `updatedAt`) VALUES
(3, 'IKLPL-KA-BAN-20-9', 'Intellikidz', 'Jamuna', '1989-04-01', '9972344545', 'amar@gmail.com', 'bangalroe', NULL, '1', '', '2020-02-18', '2021-04-18', '', 'no-img.png', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(4, 'IKLPL-KA-BAN-20-10', 'Bangalore kidz', 'Ravi Kumar', '1990-04-01', '9988998888', 'ravi@gmail.com', 'fdsf', NULL, '1', '', '2020-04-18', '2021-02-18', '', 'no-img.png', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(5, 'IKLPL-KA-BAN-20-11', 'kinderkidz', 'Rajesh', '1990-01-01', '7766554433', 'kinder@gmail.com', 'No 120, bnaglaore', NULL, '3', '', '2020-02-18', '2021-04-18', '', 'user.jpg', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(6, 'IKLPL-KA-BAN-20-12', 'Bnaglorekids', 'Karunya', '1998-01-01', '8877665544', 'jeevan@gmail.com', 'No 140, Jayanagar, Bangalore', NULL, '1', '', '2020-01-18', '2021-04-18', '', 'user.jpg', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(7, 'IKLPL-KA-BAN-20-13', 'Harish Acharya', 'Kavitha', '1994-01-01', '9972966774', 'harish@gmail.com', 'No 22, RR Nagar, Bangalroe', NULL, '3', '', '2020-02-18', '2021-04-18', '', 'kumar.jpg', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(9, 'IKLPL-KA-TUM-20-15', 'Intelkidz1', 'Ramesh', '1992-01-01', '9972966556', 'ramesh@gmail.com', 'No 444, Shivajinagar,Bangalore', NULL, '7', '', '2020-03-18', '2021-04-18', '', 'kumar.jpg', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(10, 'IKLPL-KA-MYS-20-16', 'Brilliant Intelikidz', 'Arjun', '1993-04-25', '7795139603', 'arjun@gmail.com', 'No 234, 1st cross, Mysore', NULL, '3', '', '2020-04-18', '2021-04-23', '', 'user.jpg', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(11, 'IKLPL-KA-MYS-20-17', 'Veda Intelkidz', 'Raajiv', '1991-04-25', '9972966665', 'veda@gmail.com', 'No 143, 2nd cross, Mysore', NULL, '3', '', '2020-04-25', '2021-04-23', '', 'kumar.jpg', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(13, 'IKLPL-KA-MYS-20-18', 'Mysore Kids', 'Reema', '1990-04-26', '9986776655', 'reema@gmail.com', 'No 243, 2nd cross, Mysore circle, Mysore-445590', NULL, '3', '', '2020-04-18', '2021-04-23', '', 'user.jpg', 1, '2020-04-26 13:47:15', '2020-04-26 13:47:15'),
(14, 'IKLPL-KA-TUM-20-19', 'Tumkur intellikidz', 'Jayaraj', '1990-04-26', '9977665544', 'jaya@gmail.com', 'No 348, 4th cross, Tumkur-889900', NULL, '7', '', '2020-04-18', '2021-04-23', '', 'kumar.jpg', 1, '2020-04-26 13:50:48', '2020-04-26 13:50:48'),
(15, 'IKLPL-KA-HAS-20-20', 'Hassan intellikidz', 'Kiran', '1990-04-26', '9988776633', 'kiran@gmail.com', 'No 567, 5th cross, Hassan-890989', NULL, '8', '', '2020-04-18', '2021-04-23', '', 'picking.png', 1, '2020-04-26 13:52:29', '2020-04-26 13:52:29'),
(16, 'IKLPL-KA-BEL-20-21', 'Belgum Intellikidz', 'Raaju', '2020-04-25', '9972223355', 'raaju@gmail.com', 'No 890, 3rd cross, Belgum-76767', NULL, '6', '', '2020-04-18', '2021-04-23', '', 'kumar.jpg', 1, '2020-04-26 15:56:36', '2020-04-26 15:56:36');

-- --------------------------------------------------------

--
-- Table structure for table `adm_franchise_type`
--

CREATE TABLE `adm_franchise_type` (
  `id` int(11) NOT NULL,
  `franchise_type_id` varchar(45) COLLATE utf8_bin NOT NULL,
  `franchise_type_name` varchar(255) COLLATE utf8_bin NOT NULL,
  `status` tinyint(4) DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Table structure for table `adm_state`
--

CREATE TABLE `adm_state` (
  `id` int(11) NOT NULL,
  `state_id` varchar(45) NOT NULL,
  `state_name` varchar(255) DEFAULT NULL,
  `country_id` varchar(45) DEFAULT NULL,
  `status` tinyint(4) DEFAULT 1,
  `created_date` datetime DEFAULT current_timestamp(),
  `updated_date` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='this table is linked with adm_country table';

--
-- Dumping data for table `adm_state`
--

INSERT INTO `adm_state` (`id`, `state_id`, `state_name`, `country_id`, `status`, `created_date`, `updated_date`) VALUES
(1, 'KA01', 'Karnataka', 'IN', 1, '2020-04-10 17:20:52', '2020-04-10 17:20:52');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_items`
--

CREATE TABLE `tbl_items` (
  `itemId` int(11) NOT NULL,
  `itemHeader` varchar(512) NOT NULL COMMENT 'Heading',
  `itemSub` varchar(1021) NOT NULL COMMENT 'sub heading',
  `itemDesc` text DEFAULT NULL COMMENT 'content or description',
  `itemImage` varchar(80) DEFAULT NULL,
  `isDeleted` tinyint(4) NOT NULL DEFAULT 0,
  `createdBy` int(11) NOT NULL,
  `createdDtm` datetime NOT NULL,
  `updatedDtm` datetime DEFAULT NULL,
  `updatedBy` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_items`
--

INSERT INTO `tbl_items` (`itemId`, `itemHeader`, `itemSub`, `itemDesc`, `itemImage`, `isDeleted`, `createdBy`, `createdDtm`, `updatedDtm`, `updatedBy`) VALUES
(1, 'jquery.validation.js', 'Contribution towards jquery.validation.js', 'jquery.validation.js is the client side javascript validation library authored by Jörn Zaefferer hosted on github for us and we are trying to contribute to it. Working on localization now', 'validation.png', 0, 1, '2015-09-02 00:00:00', NULL, NULL),
(2, 'CodeIgniter User Management', 'Demo for user management system', 'This the demo of User Management System (Admin Panel) using CodeIgniter PHP MVC Framework and AdminLTE bootstrap theme. You can download the code from the repository or forked it to contribute. Usage and installation instructions are provided in ReadMe.MD', 'cias.png', 0, 1, '2015-09-02 00:00:00', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_reset_password`
--

CREATE TABLE `tbl_reset_password` (
  `id` bigint(20) NOT NULL,
  `email` varchar(128) NOT NULL,
  `activation_id` varchar(32) NOT NULL,
  `agent` varchar(512) NOT NULL,
  `client_ip` varchar(32) NOT NULL,
  `isDeleted` tinyint(4) NOT NULL DEFAULT 0,
  `createdBy` bigint(20) NOT NULL DEFAULT 1,
  `createdDtm` datetime NOT NULL,
  `updatedBy` bigint(20) DEFAULT NULL,
  `updatedDtm` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_roles`
--

CREATE TABLE `tbl_roles` (
  `roleId` tinyint(4) NOT NULL COMMENT 'role id',
  `role` varchar(50) NOT NULL COMMENT 'role text'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_roles`
--

INSERT INTO `tbl_roles` (`roleId`, `role`) VALUES
(1, 'System Administrator'),
(2, 'Manager'),
(3, 'Employee'),
(4, 'Franchaise');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_users`
--

CREATE TABLE `tbl_users` (
  `userId` int(11) NOT NULL,
  `email` varchar(128) NOT NULL COMMENT 'login email',
  `password` varchar(128) NOT NULL COMMENT 'hashed login password',
  `name` varchar(128) DEFAULT NULL COMMENT 'full name of user',
  `mobile` varchar(20) DEFAULT NULL,
  `roleId` tinyint(4) NOT NULL,
  `photo` varchar(1000) NOT NULL,
  `isDeleted` tinyint(4) NOT NULL DEFAULT 0,
  `createdBy` int(11) NOT NULL,
  `createdDtm` datetime NOT NULL,
  `updatedBy` int(11) DEFAULT NULL,
  `updatedDtm` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_users`
--

INSERT INTO `tbl_users` (`userId`, `email`, `password`, `name`, `mobile`, `roleId`, `photo`, `isDeleted`, `createdBy`, `createdDtm`, `updatedBy`, `updatedDtm`) VALUES
(1, 'admin@admin.com', '$2y$10$iZTHZXKOpRIezkqI.XU9u.8TxP8aDggb0bqRrc5sAJ9YPOkAfUuk6', 'Administrator', '9890098900', 1, 'no-img.png', 0, 0, '2015-07-01 18:56:49', 1, '2020-04-10 08:01:02'),
(2, 'manager@bewithdhanu.in', '$2y$10$Gkl9ILEdGNoTIV9w/xpf3.mSKs0LB1jkvvPKK7K0PSYDsQY7GE9JK', 'Manager', '9890098900', 2, '', 0, 1, '2016-12-09 17:49:56', 1, '2017-06-19 09:22:29'),
(3, 'employee@bewithdhanu.in', '$2y$10$MB5NIu8i28XtMCnuExyFB.Ao1OXSteNpCiZSiaMSRPQx1F1WLRId2', 'Employee', '9890098900', 3, '', 0, 1, '2016-12-09 17:50:22', 1, '2017-06-19 09:23:21'),
(4, 'IKLPL-KA-DWD-08-180', '$2y$10$oZp.7pB6Hcm2aMJWS85ekOq9G.xIYCcyPepPrTECuwNdLckL.nwbi', 'IKLPL-KA-DWD-08-180', '9972966773', 2, '', 0, 1, '2020-04-10 08:21:18', NULL, NULL),
(5, 'harish1@gmail.com', '$2y$10$xGBkmILqtQLy2zltegQAx.1ldCo36TKKwD2GRGocmOXDtBUVAllFu', 'Harish', '9845480386', 3, '', 0, 1, '2020-04-10 09:05:15', NULL, NULL),
(6, 'kiran@gmail.com', '$2y$10$7XehIp3OfBeqdPSdpJmuleXHePtG7uFUdW8Y//GO/UplQAVhOVBxO', 'Kiran', '9972966773', 2, '', 0, 1, '2020-04-10 09:46:15', NULL, NULL),
(7, 'kiran1@gmail.com', '$2y$10$7XehIp3OfBeqdPSdpJmuleXHePtG7uFUdW8Y//GO/UplQAVhOVBxO', 'Kiran1', '9972966773', 2, '', 0, 1, '2020-04-10 09:46:15', NULL, NULL),
(8, 'kumar1@gmail.com', '$2y$10$7XehIp3OfBeqdPSdpJmuleXHePtG7uFUdW8Y//GO/UplQAVhOVBxO', 'Kumar1', '9972966773', 2, '', 0, 1, '2020-04-10 09:46:15', NULL, NULL),
(9, 'amar@gmail.com', '$2y$10$tWWpF2OLNKbpoJZQwHpJNuj7z2jyChjWFJR1xu1uMgcxYXj.kipyO', 'intellikidz', '9972966773', 4, 'no-img.png', 0, 1, '2020-04-25 11:25:51', NULL, NULL),
(10, 'sf', '$2y$10$7mXdTRJjrErQ21yNTd3Sz.YopB6LT7A8iL3rqIzM3pNLd7uHFQXsm', 'Fdsfs', '9972966773', 4, '', 0, 1, '2020-04-25 12:15:36', NULL, NULL),
(11, 'kinder@gmail.com', '$2y$10$lxWavIKySvPwH5hWcuWgh.eG1NUOTfxqYHxAygDnQn1yYzTSxtIIK', 'Kinderkidz', '9972966771', 4, '', 0, 1, '2020-04-25 12:18:21', NULL, NULL),
(12, 'IKLPL-KA-BAN-20-12', '$2y$10$HcpYgTs5EMNpdSZzhDniQuNj5vhFqdSaxbLhdH3y24c6amLM4/Saq', 'Bnaglorekids', '9972966772', 4, 'user.jpg', 0, 1, '2020-04-25 12:46:22', NULL, NULL),
(13, 'IKLPL-KA-BAN-20-13', '$2y$10$wUUpePgCRCqwdjXaYU6s9eP/mzbc1rWtczeMpniJbKVDACm8PJnn6', 'Harish Acharya', '9845787888', 4, 'kumar.jpg', 0, 1, '2020-04-25 13:05:05', NULL, NULL),
(16, 'IKLPL-KA-MYS-20-16', '$2y$10$3fgHIgyiCvwQdj.Ns4McJ.3VS62t.wQt4TJO06T2u.vAwG7jcNdSW', 'Brilliant Intelikidz', '7795139603', 4, 'user.jpg', 0, 1, '2020-04-25 17:06:24', NULL, NULL),
(15, 'IKLPL-KA-TUM-20-15', '$2y$10$4WN.luhOuxgEtUPPw4UHjeHA8vqcT5VfTn0lPH5mo4DOj.B1OX77q', 'Intelkidz1', '9972966556', 4, 'kumar.jpg', 0, 1, '2020-04-25 16:01:34', NULL, NULL),
(17, 'IKLPL-KA-MYS-20-17', '$2y$10$HRZak9AaXp3ZjjWw7.wi7.DFFTPh1TDOBHxskT9Iob4smF8RWuptW', 'Veda Intelkidz', '9972966664', 4, 'kumar.jpg', 0, 1, '2020-04-25 17:45:22', NULL, NULL),
(18, 'IKLPL-KA-MYS-20-18', '$2y$10$89KeSu7Vv8e8mqG86LM6i.qoyE7Ri63IYlShqyIV9JFJHpzO99Osa', 'Mysore Kids', '9986776655', 4, 'user.jpg', 0, 1, '2020-04-26 10:17:15', NULL, NULL),
(19, 'IKLPL-KA-TUM-20-19', '$2y$10$PVFzoW98uej9zEq5Xq0.K.CGJGAEYB5cuNcKTTbevylDcBIYc3j3S', 'Tumkur Intellikidz', '9977665544', 4, 'kumar.jpg', 0, 1, '2020-04-26 10:20:48', NULL, NULL),
(20, 'IKLPL-KA-HAS-20-20', '$2y$10$eSIan1lMZAGNYQF3StohWuGFxqzKrpyapA7vIJWNusJroQSKPbtn6', 'Hassan Intellikidz', '9988776633', 4, 'picking.png', 0, 1, '2020-04-26 10:22:29', NULL, NULL),
(21, 'IKLPL-KA-BEL-20-21', '$2y$10$f.iA4327k.Wgdau.cQFsA.FgzxFQRLitFWOcRoVTMMAbwGnpTKw8m', 'Belgum Intellikidz', '9972223355', 4, 'kumar.jpg', 0, 1, '2020-04-26 12:26:36', NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `adm_city`
--
ALTER TABLE `adm_city`
  ADD PRIMARY KEY (`id`),
  ADD KEY `city_state_idx` (`city_id`,`state_id`),
  ADD KEY `fk_state_id_idx` (`state_id`);

--
-- Indexes for table `adm_country`
--
ALTER TABLE `adm_country`
  ADD PRIMARY KEY (`id`,`country_id`),
  ADD KEY `country_code_idx` (`country_code`),
  ADD KEY `country_id_idx` (`country_id`);

--
-- Indexes for table `adm_franchise_master`
--
ALTER TABLE `adm_franchise_master`
  ADD PRIMARY KEY (`id`,`franchise_id`);

--
-- Indexes for table `adm_franchise_type`
--
ALTER TABLE `adm_franchise_type`
  ADD PRIMARY KEY (`id`,`franchise_type_id`);

--
-- Indexes for table `adm_state`
--
ALTER TABLE `adm_state`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_country_id_idx` (`country_id`),
  ADD KEY `state_idx` (`state_id`);

--
-- Indexes for table `tbl_items`
--
ALTER TABLE `tbl_items`
  ADD PRIMARY KEY (`itemId`);

--
-- Indexes for table `tbl_reset_password`
--
ALTER TABLE `tbl_reset_password`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_roles`
--
ALTER TABLE `tbl_roles`
  ADD PRIMARY KEY (`roleId`);

--
-- Indexes for table `tbl_users`
--
ALTER TABLE `tbl_users`
  ADD PRIMARY KEY (`userId`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `adm_city`
--
ALTER TABLE `adm_city`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `adm_country`
--
ALTER TABLE `adm_country`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `adm_franchise_master`
--
ALTER TABLE `adm_franchise_master`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `adm_franchise_type`
--
ALTER TABLE `adm_franchise_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `adm_state`
--
ALTER TABLE `adm_state`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tbl_items`
--
ALTER TABLE `tbl_items`
  MODIFY `itemId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tbl_reset_password`
--
ALTER TABLE `tbl_reset_password`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `tbl_roles`
--
ALTER TABLE `tbl_roles`
  MODIFY `roleId` tinyint(4) NOT NULL AUTO_INCREMENT COMMENT 'role id', AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tbl_users`
--
ALTER TABLE `tbl_users`
  MODIFY `userId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `adm_city`
--
ALTER TABLE `adm_city`
  ADD CONSTRAINT `fk_state_id` FOREIGN KEY (`state_id`) REFERENCES `adm_state` (`state_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `adm_state`
--
ALTER TABLE `adm_state`
  ADD CONSTRAINT `fk_country_id` FOREIGN KEY (`country_id`) REFERENCES `adm_country` (`country_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
