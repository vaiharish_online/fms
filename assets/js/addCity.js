/* * 
 * This file contain the validation of add user form
 * 
 * Using validation plugin : jquery.validate.js
 * 
 * @author Kishor Mali
 */
 
$(document).ready(function(){
	
	var addCityForm = $("#addCity");
	
	var validator = addCityForm.validate({
		
		rules:{
			cname :{ required : true },
			district : { required : true },
			code : { required : true },
			pincode : {required : true},
			state : { required : true, selected : true}
		},
		messages:{
			cname :{ required : "This field is required" },
			district : { required : "This field is required"},
			code : { required : "This field is required" },
			pincode : {required : "This field is required"},
			state : { required : "This field is required", selected : "Please select atleast one option" }			
		}
	});
});