/* * 
 * This file contain the validation of add user form
 * 
 * Using validation plugin : jquery.validate.js
 * 
 * @author Kishor Mali
 */
 
$(document).ready(function(){
	
	var editFranchiseForm = $("#editFranchise");
	
	var validator = editFranchiseForm.validate({
		
		rules:{
			cname :{ required : true },
			coordinator : { required : true },
			saddress : { required : true },
			dob : {required : true},
			mno : {required : true},
			email : {required : true},
			cdate : {required : true},
			rdate : {required : true},
			cyear : {required : true},
			btype : {required : true},
			// photo : {required : true},
			state : { required : true, selected : true}
		},
		messages:{
			cname :{ required : "This field is required" },
			coordinator : { required : "This field is required"},
			saddress : { required : "This field is required" },
			dob : {required : "This field is required"},
			mno : {required : "This field is required"},
			email : {required : "This field is required"},
			cdate : {required : "This field is required"},
			cyear : {required : "This field is required"},
			rdate : {required : "This field is required"},
			btype : {required : "This field is required"},
			// photo : {required : "This field is required"},
			state : { required : "This field is required", selected : "Please select atleast one option" }			
		}
	});
});