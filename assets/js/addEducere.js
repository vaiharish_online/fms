/* * 
 * This file contain the validation of add user form
 * 
 * Using validation plugin : jquery.validate.js
 * 
 * @author Kishor Mali
 */
 
$(document).ready(function(){
	
	var addEducereForm = $("#addEducere");
	
	var validator = addEducereForm .validate({
		
		rules:{
			fname :{ required : true },
			bgroup: { required : true },
			address : { required : true },
			dob : {required : true},
			cno : {required : true},
			email : {required : true},
			pincode : {required : true},
			qualification : {required : true},
			year : {required : true, selected : true},
			gender : {required : true},
			ldate : {required : true},
            franchisename : { required : true, selected : true},
            alevel : { required : true, selected : true}
		},
		messages:{
			fname :{ required : "This field is required" },
			bgroup: { required : "This field is required" },
			address : { required : "This field is required" },
			dob : {required : "This field is required"},
			cno : {required : "This field is required"},
			email : {required : "This field is required"},
			pincode : {required : "This field is required"},
			qualification : {required : "This field is required"},
			year : {required : "This field is required", selected : "Please select atleast one option"},
			gender : {required : "This field is required", selected : "Please select atleast one option"},
			ldate : {required : "This field is required"},
            alevel : {required : true},
			// photo : {required : "This field is required"},
			franchise_name : { required : "This field is required", selected : "Please select atleast one option" }			
		}
	});
});